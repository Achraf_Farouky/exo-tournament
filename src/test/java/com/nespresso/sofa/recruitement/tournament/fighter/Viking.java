package com.nespresso.sofa.recruitement.tournament.fighter;

public class Viking extends Warrior{
	
	
	
	public Viking() {
		super();
		this.setHitPoints(120);
		this.setDamage(6);
	}

	public void engage(Warrior W) {
		// TODO Auto-generated method stub
		
	}

	public int hitPoints() {
		return hitpoints;
	}

	public int damage() {
		return dmg;
	}

	public void setHitPoints(int p) {
		this.hitpoints=p;
		
	}

	public void setDamage(int dmg) {
		this.dmg=dmg;
		
	}

	@Override
	public Viking equip(String eq) {
		if(eq.equalsIgnoreCase("buckler"))
			this.setBuckler(true);
		if(eq.equalsIgnoreCase("armor"))
			this.setArmored(true);
		return this;
	}

	public boolean isBuckler() {
		return buckler;
	}

	public void setBuckler(boolean buckler) {
		this.buckler = buckler;
	}

	@Override
	public boolean isArmored() {
		return armor;
	}

	@Override
	public void setArmored(boolean armor) {
		this.armor = armor;
		
	}

}
