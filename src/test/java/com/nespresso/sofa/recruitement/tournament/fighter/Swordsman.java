package com.nespresso.sofa.recruitement.tournament.fighter;

public class Swordsman extends Warrior {

	public Swordsman() {
		super();
		this.setHitPoints(100);
		this.setDamage(5);
	}

	public void engage(Warrior warrior) {
		int i = 1, j = 1, bucklerCounter = 0;
		do {
			if (i % 2 != 0) {
				attack(warrior, j);
			} else {
				// the buckler is UP
				if (warrior.getClass().getSimpleName().equalsIgnoreCase("highlander")) {
					if (this.isBuckler()) {
						if (i % 6 != 0) {
							if (this.isBuckler() && j % 2 == 0)
								this.reduce(this, this.hitPoints(), warrior.damage());
						} else
							j--;
					}
				} else {
					if (this.isBuckler() && bucklerCounter < 3) {
						if (i % 6 != 0) {
							if (this.isBuckler() && j % 2 == 0) {
								this.reduce(this, this.hitPoints(), warrior.damage());
							} else {
								bucklerCounter++;
							}

						} else j--;
						// the buckler is down
					} else {
						this.reduce(this, this.hitPoints(), warrior.damage());
					}
				}

				j++;

			}
			i++;
			if (this.hitPoints() <= 0)	this.setHitPoints(0);
		} while (this.hitPoints() > 0 && warrior.hitPoints() > 0);

	}

	private void attack(Warrior warrior, int j) {
		if (warrior.isBuckler()  && j % 2 == 0) {
			this.reduce(warrior, warrior.hitPoints(), this.damage());
		} else if (!warrior.isBuckler()) {
			this.reduce(warrior, warrior.hitPoints(), this.damage());
		}
	}

	public int hitPoints() {
		return hitpoints;
	}

	public int damage() {
		return dmg;
	}

	public void setHitPoints(int p) {
		this.hitpoints = p;

	}

	public void setDamage(int dmg) {
		this.dmg = dmg;

	}

	@Override
	public Swordsman equip(String eq) {
		if (eq.equalsIgnoreCase("buckler"))
			this.setBuckler(true);
		if (eq.equalsIgnoreCase("armor"))
			this.setArmored(true);
		return this;
	}

	public boolean isBuckler() {
		return buckler;
	}

	public void setBuckler(boolean buckler) {
		this.buckler = buckler;
	}

	@Override
	public boolean isArmored() {
		return armor;
	}

	@Override
	public void setArmored(boolean armor) {
		if (armor == true) {
			this.setDamage(this.damage() - 1);
		}
		this.armor = armor;

	}

	public void reduce(Warrior W, int points, int dmg) {
		if (W.isArmored() == true)
			W.setHitPoints(points - (dmg - 3));
		else
			W.setHitPoints(points - dmg);
	}

}
