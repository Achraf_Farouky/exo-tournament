package com.nespresso.sofa.recruitement.tournament.fighter;

public class Highlander extends Warrior{
	
	

	public Highlander() {
		super();
		this.setHitPoints(150);
		this.setDamage(12);
	}

	@Override
	public void engage(Warrior W) {
	}

	@Override
	public int hitPoints() {
		return hitpoints;
	}

	@Override
	public void setHitPoints(int p) {
		this.hitpoints=p;
		
	}

	@Override
	public int damage() {
		return dmg;
	}

	@Override
	public void setDamage(int dmg) {
		this.dmg=dmg;
		
	}

	@Override
	public Warrior equip(String eq) {
		if(eq.equalsIgnoreCase("buckler"))
			this.setBuckler(true);
		if(eq.equalsIgnoreCase("armor"))
			this.setArmored(true);
		return this;
	}

	@Override
	public boolean isBuckler() {
		return buckler;
	}

	@Override
	public void setBuckler(boolean buckler) {
		this.buckler=buckler;
		
	}
	
	@Override
	public boolean isArmored() {
		return armor;
	}

	@Override
	public void setArmored(boolean armor) {
		this.armor = armor;
		
	}

}
