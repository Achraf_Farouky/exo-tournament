package com.nespresso.sofa.recruitement.tournament.fighter;

public abstract class Warrior {

	protected int hitpoints,dmg;
	
	protected boolean buckler,armor;
	
	public abstract void engage(Warrior W);

	public abstract int hitPoints();
	
	public abstract void setHitPoints(int p);
	
	public abstract int damage();
	
	public abstract void setDamage(int dmg);
	
	public abstract Warrior equip(String eq);
	
	public abstract boolean isBuckler();

	public abstract void setBuckler(boolean buckler);
	
	public abstract boolean isArmored();

	public abstract void setArmored(boolean armor);
	
}
